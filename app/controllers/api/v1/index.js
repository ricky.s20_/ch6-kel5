/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const userController = require("./userController");
const toppingController = require("./toppingController");
const drinkController = require("./drinkController");
const orderController = require("./orderController");
const billController = require("./billController");
const authController = require("./authController");

module.exports = {
  userController,  toppingController, drinkController, orderController, billController, authController
};
