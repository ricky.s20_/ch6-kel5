/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const postService = require("../../../services/userService");
const bcrypt = require("bcryptjs");
const userService = require("../../../services/userService");
const jwt = require("jsonwebtoken")
const SALT = 10;

function encryptPassword(password) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, SALT, (err, encryptedPassword) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(encryptedPassword);
    });
  });
}

function checkPassword(encryptedPassword, password) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(isPasswordCorrect);
    });
  });
}

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
}

module.exports = {
  list(req, res) {
    postService
      .list()
      .then(({ data, count }) => {
        res.status(200).json({
          status: "OK",
          data: { posts: data },
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async create(req, res) {
    // const email = req.body.email;
    const encryptedPassword = await encryptPassword(req.body.password);
    postService
      .create({...req.body, password:encryptedPassword})
      .then((post) => {
        res.status(201).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async update(req, res) 
  {
    const encryptedPassword = await encryptPassword(req.body.password);
    postService
      .update(req.params.id,{...req.body, password:encryptedPassword})
      .then(() => {
        res.status(200).json({
          status: "OK",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    postService
      .get(req.params.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  destroy(req, res) {
    postService
      .delete({where : {
        id : req.params.id}
      })
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async login(req, res) {
    const email = req.body.email.toLowerCase(); // Biar case insensitive
    const password2 = req.body.password;

    const loginuser = await userService.login(email)

    if (!loginuser) {
      res.status(404).json({ message: "Email tidak ditemukan" });
      return;
    }

    const isPasswordCorrect = await checkPassword(
      loginuser.password,
      password2
    );

    if (!isPasswordCorrect) {
      res.status(401).json({ message: "Password salah!" });
      return;
    }

    const token = createToken({
      id: loginuser.id,
      username: loginuser.fullName,
      email: loginuser.email,
      address: loginuser.address,
      createdAt: loginuser.createdAt,
      updatedAt: loginuser.updatedAt,
    });

    res.status(200).json({
      id: loginuser.id,
      email: loginuser.email,
      token, // Kita bakal ngomongin ini lagi nanti.
      createdAt: loginuser.createdAt,
      updatedAt: loginuser.updatedAt,
    });
  },

  async authorize(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      // const token = bearerToken;
      const tokenPayload = jwt.verify(
        bearerToken,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      req.loginuser = await userService.get(tokenPayload.id);
      next();
    } catch (err) {
      console.error(err);
      res.status(401).json({
        message: "Unauthorized",
      });
    }
  },
};
