const toppingRepo = require("../repositories/toppingRepo");

module.exports = {
  create(requestBody) {
    return toppingRepo.create(requestBody);
  },

  update(id, requestBody) {
    return toppingRepo.update(id, requestBody);
  },

  delete(id) {
    return toppingRepo.delete(id);
  },

  async list() {
    try {
      const posts = await toppingRepo.findAll();
      const postCount = await toppingRepo.getTotalPost();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return toppingRepo.find(id);
  },
};
