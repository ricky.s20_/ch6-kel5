const orderRepo = require("../repositories/orderRepo");

module.exports = {
  create(requestBody) {
    return orderRepo.create(requestBody);
  },

  update(id, requestBody) {
    return orderRepo.update(id, requestBody);
  },

  delete(id) {
    return orderRepo.delete(id);
  },

  async list() {
    try {
      const posts = await orderRepo.findAll();
      const postCount = await orderRepo.getTotalPost();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return orderRepo.find(id);
  },
};
