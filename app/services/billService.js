const billRepo = require("../repositories/billRepo");

module.exports = {
  create(requestBody) {
    return billRepo.create(requestBody);
  },

  update(id, requestBody) {
    return billRepo.update(id, requestBody);
  },

  delete(id) {
    return billRepo.delete(id);
  },

  async list() {
    try {
      const posts = await billRepo.findAll();
      const postCount = await billRepo.getTotalPost();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return billRepo.find(id);
  },
};
