const drinkRepo = require("../repositories/drinkRepo");

module.exports = {
  create(requestBody) {
    return drinkRepo.create(requestBody);
  },

  update(id, requestBody) {
    return drinkRepo.update(id, requestBody);
  },

  delete(id) {
    return drinkRepo.delete(id);
  },

  async list() {
    try {
      const posts = await drinkRepo.findAll();
      const postCount = await drinkRepo.getTotalPost();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return drinkRepo.find(id);
  },
};
