'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class orderItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      orderItem.belongsTo(models.user,{
        foreignKey:"id_user"
      });
      orderItem.belongsTo(models.drink,{
        foreignKey:"id_drink"
      });
      orderItem.belongsTo(models.topping,{
        foreignKey:"id_topping"
      });
    }
  }
  orderItem.init({
    id_user: DataTypes.INTEGER,
    id_drink: DataTypes.INTEGER,
    id_topping: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'orderItem',
  });
  return orderItem;
};