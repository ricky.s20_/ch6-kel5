'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bill extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      bill.belongsTo(models.orderItem,{
        foreignKey:"id_orderitem"
      });
    }
  }
  bill.init({
    id_orderitem: DataTypes.INTEGER,
    count: DataTypes.INTEGER,
    price: DataTypes.INTEGER,
    method_bill: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'bill',
  });
  return bill;
};