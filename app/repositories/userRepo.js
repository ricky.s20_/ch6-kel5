const { user } = require("../models");

module.exports = {
  create(createArgs) {
    return user.create(createArgs);
  },

  login(email){
    return user.findOne({
      where: { email },
    })
  },

  update(id, updateArgs) {
    return user.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return user.destroy(id);
  },

  find(id) {
    return user.findByPk(id);
  },

  findAll() {
    return user.findAll();
  },

  getTotalPost() {
    return user.count();
  },
};
