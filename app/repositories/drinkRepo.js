const { drink } = require("../models");

module.exports = {
  create(createArgs) {
    return drink.create(createArgs);
  },

  update(id, updateArgs) {
    return drink.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return drink.destroy(id);
  },

  find(id) {
    return drink.findByPk(id);
  },

  findAll() {
    return drink.findAll();
  },

  getTotalPost() {
    return drink.count();
  },
};
