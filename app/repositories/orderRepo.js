const { orderItem } = require("../models");
const { user } = require("../models");
const { drink } = require("../models");
const { topping } = require("../models");

module.exports = {
  create(createArgs) {
    return orderItem.create(createArgs);
  },

  update(id, updateArgs) {
    return orderItem.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return orderItem.destroy(id);
  },

  find(id) {
    return orderItem.findByPk(id, {
        include :[
          {
            model:user,
          },
          {
            model:drink,
          },
          {
            model:topping,
          },
        ],
      });
  },

  findAll() {
    return orderItem.findAll({
        include :[
          {
            model:user,
          },
          {
            model:drink,
          },
          {
            model:topping,
          },
        ],
      });
  },

  getTotalPost() {
    return orderItem.count();
  },
};
