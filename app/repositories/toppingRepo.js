const { topping } = require("../models");

module.exports = {
  create(createArgs) {
    return topping.create(createArgs);
  },

  update(id, updateArgs) {
    return topping.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return topping.destroy(id);
  },

  find(id) {
    return topping.findByPk(id);
  },

  findAll() {
    return topping.findAll();
  },

  getTotalPost() {
    return topping.count();
  },
};
