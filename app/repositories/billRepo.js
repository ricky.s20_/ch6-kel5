const { bill } = require("../models");
const { orderItem } = require("../models");
// const { user } = require("../models");
// const { drink } = require("../models");
// const { topping } = require("../models");

module.exports = {
  create(createArgs) {
    return bill.create(createArgs);
  },

  update(id, updateArgs) {
    return bill.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return bill.destroy(id);
  },

  find(id) {
    return bill.findByPk(id, {
        include :[
          {
            model:orderItem,
          },
        ],
      });
  },

  findAll() {
    return bill.findAll({
        include :[
          {
            model:orderItem,
          },
        ],
      });
  },

  getTotalPost() {
    return bill.count();
  },
};
