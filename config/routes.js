const express = require("express");
const controllers = require("../app/controllers");

const apiRouter = express.Router();

//login and register
apiRouter.post("/api/v1/user/register", controllers.api.v1.userController.create);
apiRouter.post("/api/v1/user/login", controllers.api.v1.userController.login);

// api with auth
apiRouter.get("/api/v1/user", controllers.api.v1.userController.authorize, controllers.api.v1.userController.list);
apiRouter.put("/api/v1/user/:id", controllers.api.v1.userController.authorize, controllers.api.v1.userController.update);
apiRouter.get("/api/v1/user/:id", controllers.api.v1.userController.authorize, controllers.api.v1.userController.show);
apiRouter.delete(
  "/api/v1/user/:id",
  controllers.api.v1.userController.authorize, controllers.api.v1.userController.destroy
);

apiRouter.get("/api/v1/topping",controllers.api.v1.userController.authorize, controllers.api.v1.toppingController.list);
apiRouter.post("/api/v1/topping",controllers.api.v1.userController.authorize, controllers.api.v1.toppingController.create);
apiRouter.put("/api/v1/topping/:id",controllers.api.v1.userController.authorize, controllers.api.v1.toppingController.update);
apiRouter.get("/api/v1/topping/:id",controllers.api.v1.userController.authorize, controllers.api.v1.toppingController.show);
apiRouter.delete(
  "/api/v1/topping/:id",
  controllers.api.v1.userController.authorize, controllers.api.v1.toppingController.destroy
);

apiRouter.get("/api/v1/drink",controllers.api.v1.userController.authorize, controllers.api.v1.drinkController.list);
apiRouter.post("/api/v1/drink",controllers.api.v1.userController.authorize, controllers.api.v1.drinkController.create);
apiRouter.put("/api/v1/drink/:id",controllers.api.v1.userController.authorize, controllers.api.v1.drinkController.update);
apiRouter.get("/api/v1/drink/:id",controllers.api.v1.userController.authorize, controllers.api.v1.drinkController.show);
apiRouter.delete(
  "/api/v1/drink/:id",
  controllers.api.v1.userController.authorize,controllers.api.v1.drinkController.destroy
);


apiRouter.get("/api/v1/order",controllers.api.v1.userController.authorize, controllers.api.v1.orderController.list);
apiRouter.post("/api/v1/order",controllers.api.v1.userController.authorize, controllers.api.v1.orderController.create);
apiRouter.put("/api/v1/order/:id",controllers.api.v1.userController.authorize, controllers.api.v1.orderController.update);
apiRouter.get("/api/v1/order/:id",controllers.api.v1.userController.authorize, controllers.api.v1.orderController.show);
apiRouter.delete(
  "/api/v1/order/:id",
  controllers.api.v1.userController.authorize,controllers.api.v1.orderController.destroy
);

apiRouter.get("/api/v1/bill",controllers.api.v1.userController.authorize, controllers.api.v1.billController.list);
apiRouter.post("/api/v1/bill",controllers.api.v1.userController.authorize, controllers.api.v1.billController.create);
apiRouter.put("/api/v1/bill/:id",controllers.api.v1.userController.authorize, controllers.api.v1.billController.update);
apiRouter.get("/api/v1/bill/:id",controllers.api.v1.userController.authorize, controllers.api.v1.billController.show);
apiRouter.delete(
  "/api/v1/bill/:id",
  controllers.api.v1.userController.authorize, controllers.api.v1.billController.destroy
);

// apiRouter.post("/api/v1/login", controllers.api.v1.authController.login);
// apiRouter.post("/api/v1/register", controllers.api.v1.authController.register);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
